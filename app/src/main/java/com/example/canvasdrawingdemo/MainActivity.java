package com.example.canvasdrawingdemo;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Bundle;
import android.widget.ImageView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //make our variables
        ImageView imageView = findViewById(R.id.imageView);
        Bitmap b = Bitmap.createBitmap(300 ,500 ,Bitmap.Config.ARGB_8888);

        //2. Setup the canvas
        Canvas canvas = new Canvas(b);

        //3. Set up your paint brush
        Paint paintBrush = new Paint();

        //draw some stuff on the canvas
        //--------------

        //1. set the background color
        canvas.drawColor(Color.RED);

        //2. set the paintbrush color
        paintBrush.setColor(Color.WHITE);

        //3. draw a straight line
        canvas.drawLine(10, 50, 200, 50, paintBrush);

        //4. draw a diagonal line
        canvas.drawLine(10, 50, 200, 150, paintBrush);

        //Draw some squares

        //1. set crayon color
        paintBrush.setColor(Color.WHITE);

        //2. Draw a 20*20 square
        canvas.drawRect(100, 100, 120, 120, paintBrush);

        //3. change crayon color
        paintBrush.setColor(Color.YELLOW);

        //4. Draw a 50*50 square
        canvas.drawRect(150, 150, 200, 200, paintBrush);

        // Draw some texts
        //1. set the text size
        paintBrush.setTextSize(40);

        //2. Draw text onto screen
        canvas.drawText("Hello World", 10, 400, paintBrush);

        //3. Draw some more text
        paintBrush.setTextSize(30);
        paintBrush.setColor(Color.BLACK);
        canvas.drawText("Goodbye World", 10, 450, paintBrush);
        //put the canvas into the frame
        //---------------
        imageView.setImageBitmap(b);
    }
}
